import _ from 'lodash'

export const state = () => ({
  now: new Date(),
  nowInterval: null,
  liveOffset: 5 * 60 * 1000,
  navigation: {
    state: false,
    items: {
      primary: [
        {
          title: 'Live',
          to: '/'
        },
        {
          title: 'Program',
          to: '/program'
        },
        {
          title: 'Channels',
          to: '/channels'
        },
        {
          title: 'Recordings',
          to: '/recordings'
        },
        {
          title: 'About',
          to: '/about'
        }
      ],
      footer: [
        {
          title: 'Privacy',
          to: '/privacy'
        },
        {
          title: 'Imprint',
          to: '/imprint'
        }
      ]
    }
  },
  socialLinks: [
    {
      title: 'Instagram',
      uri: 'https://www.instagram.com/kapsel.space/'
    },
    {
      title: 'Telegram',
      uri: 'https://t.me/kapselspace'
    }
  ],
  backupStreams: [
    {
      title: 'PJZ: Watch an empire being built',
      field_embed_code: {
        value:
          '<iframe src="https://player.twitch.tv/?channel=exitfilmkollektiv" frameborder="0" allowfullscreen="true" scrolling="no" height="378" width="620"></iframe>'
      },
      field_links: [
        {
          title: 'exit',
          uri: 'http://exit.ist/'
        }
      ]
    }
    /* , {
      title: 'Falkenkamera Josefstrasse aussen',
      field_embed_code: {
        value:
          '<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/fYC-LF4iGKA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
      },
      field_links: [
        {
          title: ' GSZ Wildlife',
          uri: 'https://www.youtube.com/channel/UCrG48vXpr_dsp8v0RW0lYbg'
        }
      ]
    } */
  ]
})

export const mutations = {
  setNavigationState(state, payload) {
    state.navigation.state = payload
  },

  updateTime(state) {
    state.now = new Date()
  },

  setNowInterval(state, payload) {
    state.nowInterval = payload
  }
}

export const actions = {
  async setNavigationState(context, payload) {
    await context.commit('setNavigationState', payload)
  },

  startInterval(context) {
    if (context.state.nowInterval === null) {
      const nowInterval = setInterval(() => {
        context.commit('updateTime')
      }, 1000 * 10)
      context.commit('setNowInterval', nowInterval)
    }
  }
}

export const getters = {
  navigation(state) {
    return state.navigation
  },
  socialLinks(state) {
    return state.socialLinks
  },
  randomBackupStreams(state) {
    const shuffledBackupStreams = _.shuffle(state.backupStreams)

    return [shuffledBackupStreams[0]]
  }
}
