import _ from 'lodash'
import listBase from '@/util/listBase'

const now = new Date()
now.setMilliseconds(0)
now.setSeconds(0)
now.setMinutes(0)

const dateIn1Day = new Date(now.getTime() + 1000 * 60 * 60 * 24)

const defaultQuery = {
  filter: {
    status: {
      value: 1
    },
    field_show_in_recordings: {
      value: 1
    },
    field_date_range_end: {
      path: 'field_date_range.end_value',
      operator: '<',
      value: dateIn1Day.toISOString()
    }
  },
  include: [
    'field_image_media,field_image_media,field_image_media.field_media_image,uid'
  ],
  fields: {
    'node--stream': [
      'title,path,field_image,field_date_range,field_embed_code,uid,field_text,field_links,uid,field_image_media'
    ],
    'file--file': ['uri,meta'],
    'user--user': ['display_name,path'],
    'media--image': ['field_media_image,created']
  },
  sort: {
    changed: {
      path: 'field_date_range.value',
      direction: 'DESC'
    }
  }
}

const endpoint = 'node/stream'

export const state = listBase.state
export const mutations = listBase.mutations
export const actions = {
  ...listBase.actions,
  async fetchItems(context, payload = {}) {
    payload.endpoint = endpoint
    const query = _.cloneDeep(defaultQuery)
    payload.query = query
    const result = await context.dispatch('fetchItemsBase', payload)
    return result
  }
}

export const getters = {
  ...listBase.getters,
  itemsEnded(state, getters, rootState) {
    return state.allItems.items.filter((item) => {
      const end = new Date(item.field_date_range.end_value)
      const hasEnded = end.getTime() < rootState.global.now.getTime()
      return hasEnded
    })
  },
  itemsEndedOffset(state, getters, rootState) {
    return state.allItems.items.filter((item) => {
      const dateAhead = new Date(
        rootState.global.now.getTime() + rootState.global.liveOffset
      )
      const end = new Date(item.field_date_range.end_value)
      const hasEnded = end.getTime() < dateAhead.getTime()
      return hasEnded
    })
  }
}
