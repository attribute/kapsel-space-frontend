import _ from 'lodash'
import listBase from '@/util/listBase'
import formatDate from '@/util/formatDate'

const now = new Date()
now.setMilliseconds(0)
now.setSeconds(0)
now.setMinutes(0)

const defaultQuery = {
  filter: {
    status: {
      value: 1
    },
    field_date_range_start: {
      path: 'field_date_range.end_value',
      operator: '>',
      value: now.toISOString()
    }
  },
  include: ['field_image,uid'],
  fields: {
    'node--stream': [
      'title,path,field_image,field_date_range,field_embed_code,field_text,field_links,uid'
    ],
    'file--file': ['uri,meta'],
    'user--user': ['display_name,path']
  },
  sort: {
    changed: {
      path: 'field_date_range.value',
      direction: 'ASC'
    }
  }
}

const endpoint = 'node/stream'

export const state = listBase.state
export const mutations = listBase.mutations
export const actions = {
  ...listBase.actions,
  async fetchItems(context, payload = {}) {
    payload.endpoint = endpoint
    const query = _.cloneDeep(defaultQuery)
    payload.query = query
    const result = await context.dispatch('fetchItemsBase', payload)
    return result
  }
}

export const getters = {
  ...listBase.getters,
  itemsByDate(state, getters) {
    const itemsInFuture = getters.itemsNotEnded

    const items = new Map()
    for (const key in itemsInFuture) {
      const item = itemsInFuture[key]
      const date = new Date(item.field_date_range.value)
      const year = date.getFullYear()
      const month = date.getMonth()
      const day = date.getDate()
      const yearMonthDay = `${year}-${month}-${day}`
      let currentMonth
      if (items.has(yearMonthDay)) {
        currentMonth = items.get(yearMonthDay)
      } else {
        currentMonth = {
          title: formatDate(date, 'long_date'),
          items: new Map()
        }
        items.set(yearMonthDay, currentMonth)
      }
      currentMonth.items.set(key, item)
    }

    return items
  },
  itemsInFuture(state, getters, rootState) {
    return state.allItems.items.filter((item) => {
      const start = new Date(item.field_date_range.value)
      const hasNotStarted = start.getTime() > rootState.global.now.getTime()
      return hasNotStarted
    })
  },
  itemsInFutureOffset(state, getters, rootState) {
    return state.allItems.items.filter((item) => {
      const dateAhead = new Date(
        rootState.global.now.getTime() + rootState.global.liveOffset
      )
      const start = new Date(item.field_date_range.value)
      const hasNotStarted = start.getTime() > dateAhead.getTime()
      return hasNotStarted
    })
  },
  itemsNotEnded(state, getters, rootState) {
    return state.allItems.items.filter((item) => {
      const end = new Date(item.field_date_range.end_value)
      const hasNotEnded = end.getTime() > rootState.global.now.getTime()
      return hasNotEnded
    })
  }
}
