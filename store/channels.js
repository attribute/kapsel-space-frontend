import _ from 'lodash'
import listBase from '@/util/listBase'

const defaultQuery = {
  filter: {
    image: {
      condition: {
        path: 'field_image.id',
        operator: 'IS NOT NULL'
      }
    }
  },
  include: ['field_image'],
  fields: {
    'user--user': [
      'display_name,field_text,field_links,field_donate_link,path,field_image'
    ]
  },
  sort: {
    changed: {
      path: 'changed',
      direction: 'DESC'
    }
  }
}

const endpoint = 'user/user'

export const state = listBase.state
export const mutations = listBase.mutations
export const actions = {
  ...listBase.actions,
  async fetchItems(context, payload = {}) {
    payload.endpoint = endpoint
    const query = _.cloneDeep(defaultQuery)
    payload.query = query
    const result = await context.dispatch('fetchItemsBase', payload)
    return result
  }
}
export const getters = listBase.getters
