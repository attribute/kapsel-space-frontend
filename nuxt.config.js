require('dotenv').config()
const aspectRatio = require('v-aspect-ratio/dist/v-aspect-ratio.ssr')

module.exports = {
  mode: 'spa',
  fallback: true,
  vue: {
    config: {
      performance: true
    }
  },
  server: {
    // port: 8000, // default: 3000
    // host: '0.0.0.0' // default: localhost
  },
  /*
   ** Setting up environment variables
   ** @TODO: remove once not needed by now anymore.
   */
  env: {
    HOME_OFFICE_API_SERVER:
      process.env.HOME_OFFICE_API_SERVER || 'https://backend.kapsel.space',
    HOME_OFFICE_APP_URL:
      process.env.HOME_OFFICE_APP_URL || 'https://kapsel.space'
  },
  /*
   ** Headers of the page
   */
  head: {
    titleTemplate: '%s - ' + process.env.npm_package_name,
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'author', content: 'kapsel.space' },
      {
        property: 'og:image',
        content: 'https://kapsel.space/icon.png'
      },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href: 'https://use.typekit.net/fne7dvm.css'
      }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#666' },

  /*
   ** Page transitions
   */
  transition: 'default',

  /*
   ** Global CSS
   */
  css: ['@/assets/sass/styles.scss'],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: '@/plugins/axios.js' },
    { src: '@/plugins/vue-infinite-loading.js', ssr: false },
    { src: '@/plugins/vue-lazysizes.client.js', ssr: false },
    { src: '~/plugins/vue-matomo.js', ssr: false }
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [],
  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/dotenv',
    '@nuxtjs/proxy',
    '@nuxtjs/style-resources',
    '@nuxtjs/svg-sprite'
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL:
      process.env.HOME_OFFICE_API_SERVER || 'https://backend.kapsel.space'
  },

  /* proxy: {
    '/sitemap.xml': process.env.HOME_OFFICE_API_SERVER,
    '/sitemap.xsl': process.env.HOME_OFFICE_API_SERVER
  }, */

  /*
   ** Sass setup
   */
  styleResources: {
    scss: ['@/assets/sass/tools.scss']
  },

  /*
   ** SVG sprite setup
   */
  svgSprite: {
    input: '~/assets/images'
  },

  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      if (ctx.isDev) {
        config.devtool = ctx.isClient ? 'source-map' : 'inline-source-map'
      }
    },
    postcss: {
      plugins: {
        'postcss-plugin-px2rem': {
          rootValue: 16
        }
      },
      preset: {
        autoprefixer: {
          grid: true
        }
      }
    }
  },
  render: {
    bundleRenderer: {
      directives: {
        'aspect-ratio': aspectRatio.default
      }
    }
  }
}
