import moment from 'moment'
import formatDate from '@/util/formatDate'

export default {
  computed: {
    formattedDateLong() {
      return (
        formatDate(this.date.value, 'long') +
        ' - ' +
        formatDate(this.date.end_value, 'time')
      )
    },
    formattedDateDay() {
      return formatDate(this.date.value, 'date_day')
    },
    formattedDateWeekday() {
      return formatDate(this.date.value, 'weekday_short')
    },
    formattedDate() {
      if (this.date === null) {
        return 'EMPTY DATE!'
      }
      return formatDate(this.date)
    },
    formattedDateRange() {
      return (
        formatDate(this.date.value, 'long') +
        ' - ' +
        formatDate(this.date.end_value, 'time')
      )
    },
    formattedDateRangeTime() {
      return (
        formatDate(this.date.value, 'time') +
        ' - ' +
        formatDate(this.date.end_value, 'time')
      )
    }
  }
}
