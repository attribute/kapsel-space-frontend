import jsonapiParse from 'jsonapi-parse'

export default function({ $axios, redirect }) {
  /* $axios.onError((error) => {
     if (error.response.status === 500) {
      redirect('/sorry')
    } 
  }) */
  $axios.onResponse((response) => {
    if (response.data) {
      return jsonapiParse.parse(response.data).data
    } else {
      return jsonapiParse.parse(response)
    }
  })
  $axios.onRequest((config) => {
    if (process.server === true) {
      config.headers.Origin = 'http://localhost:3000'
    }
  })
}
