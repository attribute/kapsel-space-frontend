import lazySizes from 'lazysizes'
import 'lazysizes/plugins/aspectratio/ls.aspectratio'

window.lazySizesConfig = window.lazySizesConfig || {}
window.lazySizesConfig.expFactor = 3
window.lazySizesConfig.loadMode = 3
window.lazySizesConfig.loadHidden = false

export default lazySizes
