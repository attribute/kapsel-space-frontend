# kapsel.space frontend

This repository contains the frontend for [kapsel.space](https://kapsel.space/). It is built using [Nuxt.js](https://nuxtjs.org) and [Vue.js](https://vuejs.org/) and consumes data using jsonapi from [kapsel-space-backend](https://gitlab.com/attribute/kapsel-space-backend), the backend based on [Drupal](https://www.drupal.org/).

## Requirements

* [node](https://nodejs.org/) (tested with 12.18.3)

## Installation

``` bash
# Install dependencies
$ npm ci

# Generate static project
$ npm run generate

# Start webserver
$ npm run start
```

## Development

``` bash
# Serve with hot reload at localhost:3000
$ npm run dev
```

## Setting a different API endpoint
You can copy `.env.example` to `.env` and edit the API endpoint. Restarting the build process is needed to apply this change.
