import '@/util/moment-de'
import moment from 'moment'

export default function formatDate(date, format = 'short') {
  const dateMoment = moment(date)
  if (typeof date === 'string') {
    date = new Date(date)
  }

  let options = {}

  switch (format) {
    case 'short':
      options = {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric'
      }
      break
    case 'long':
      options = {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric',
        hour: '2-digit',
        minute: '2-digit'
      }
      break
    case 'long_date':
      return dateMoment.format('dd, DD.MM.YYYY')
    case 'date_day':
      options = {
        day: 'numeric'
      }
      break
    case 'weekday_short':
      options = {
        weekday: 'short'
      }
      break
    case 'time':
      return dateMoment.format('HH:mm')
    case 'month_number':
      options = {
        month: 'numeric'
      }
      break
    case 'month_year':
      options = {
        month: 'long',
        year: 'numeric'
      }
      break
    default:
      break
  }
  return date.toLocaleDateString('de-DE', options)
}
