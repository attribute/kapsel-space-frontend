import moment from 'moment'
import 'moment-timezone'

moment.locale('de')

moment.tz.setDefault("Europe/Zurich")

moment.updateLocale('de', {
    weekdays : [
        "Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag",
    ],
    months: [
        "Januar", "Februar", 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'
    ]
})
